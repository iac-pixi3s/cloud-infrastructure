resource "hcloud_network" "network" {
  name     = var.network_name
  ip_range = var.network_ip_range
}

resource "hcloud_network_subnet" "network-subnet" {
  type         = var.type_network
  network_id   = hcloud_network.network.id
  network_zone = var.datacenter_zone
  ip_range     = var.subnet_ip_range
}
