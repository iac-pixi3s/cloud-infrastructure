variable "network_name" {
  default = "my-network"
}

variable "network_ip_range" {
  default = "10.0.0.0/16"
}

variable "subnet_ip_range" {
  default = "10.0.1.0/24"
}

variable "datacenter_zone" {
  default = "eu-central"
}
