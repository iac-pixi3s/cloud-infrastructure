
data "hcloud_network" "network" {
  id         = "${hcloud_network.network.id}"
  ip_range   = "${hcloud_network.network.ip_range}"
}
