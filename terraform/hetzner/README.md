# Cloud-Infrastructure

Tudo necessário para subir minha infraestutura na Hetzner usando Terraform.

# Estrutura deste repo

```
cd /home/cflb/Labs/cloud-infrastructure/terraform
tree hetzner
hetzner
└── README.md
```

# Modelo

![alt](assets/imgs/cloud-infrastructure-terraform-pipeline-hetzner-cloud.drawio.svg)