# Cloud-Infrastructure

Tudo necessário para subir minha infraestutura onde preciso.
Usando Terraform ou Crossplane

# Estrutura deste repo

```
.
├── crossplane
│   └── aws
├── README.md
└── terraform
    ├── aws
    └── ketzner
```
